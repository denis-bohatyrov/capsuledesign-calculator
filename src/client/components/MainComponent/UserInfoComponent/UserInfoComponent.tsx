import { Theme } from "@material-ui/core";
import { ContentWrapperComponent, withStyles, WithStyles } from "client/ui";
import React, { Component } from "react";

import { ApartmentFormComponent } from "./ApartmentFormComponent";
import { UserFormComponent } from "./UserFromComponent";

const styles = (theme: Theme) => ({
    container: {
        margin: "5% 5% 0",
    },
    wrapper: {
        display: "flex",
        flex: 1,
    },
});

interface IUserInfoProps extends WithStyles<typeof styles> {}

@withStyles(styles)
export class UserInfoComponent extends Component<IUserInfoProps> {
    public render() {
        const { classes } = this.props;
        return (
            <div className={classes.container}>
                <ContentWrapperComponent className={classes.wrapper}>
                    <UserFormComponent />
                    <ApartmentFormComponent />
                </ContentWrapperComponent>
            </div>
        );
    }
}
