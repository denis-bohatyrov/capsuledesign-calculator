import { CapsuleStore } from "./CapsuleStore";
import { UserInfoStore } from "./UserInfoStore";

export * from "./CapsuleStore";
export * from "./UserInfoStore";

type Store = CapsuleStore | UserInfoStore;

export interface IStoreMap {
    [key: string]: Store;
}

export function createStores(): IStoreMap {
    return {
        [CapsuleStore.key]: new CapsuleStore(),
        [UserInfoStore.key]: new UserInfoStore(),
    };
}
